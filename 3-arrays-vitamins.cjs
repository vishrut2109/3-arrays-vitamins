const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

/*
  
      1. Get all items that are available 
      2. Get all items containing only Vitamin C.
      3. Get all items containing Vitamin A.
      4. Group items based on the Vitamins that they contain in the following format:
          {
              "Vitamin C": ["Orange", "Mango"],
              "Vitamin K": ["Mango"],
          }
          
          and so on for all items and all Vitamins.
      5. Sort items based on number of Vitamins they contain.
  
      NOTE: Do not change the name of this file
  
  */

// 1 . Get all items that are available
const itemsAvailable = items.filter((fruit) => {
  if (fruit.available) {
    return fruit;
  }
});
// console.log(itemsAvailable);

// 2. Get all items containing only Vitamin C.
let vitamins;
const onlyVitaminC = items.filter((fruit) => {
  vitamins = fruit.contains;
  let vitaminsArray = vitamins.replace(/\s*,\s*/g, ",").split(",");
  if (vitaminsArray.includes("Vitamin C") && vitaminsArray.length === 1) {
    return fruit;
  }
});
// console.log(onlyVitaminC);

//  3. Get all items containing Vitamin A.
const vitaminA = items.filter((fruit) => {
  vitamins = fruit.contains;
  if (vitamins.includes("Vitamin A")) {
    return fruit;
  }
});
// console.log(vitaminA);

//  4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

let groupByVitamin = {};
items.map((fruit) => {
  vitamins = fruit.contains;
  let vitaminsArray = vitamins.replace(/\s*,\s*/g, ",").split(",");
  vitaminsArray.map((entry) => {
    if (groupByVitamin[entry]) {
      groupByVitamin[entry].push(fruit.name);
    } else {
      groupByVitamin[entry] = [];
      groupByVitamin[entry].push(fruit.name);
    }
  });
});
// console.log(groupByVitamin);

//  5. Sort items based on number of Vitamins they contain.

const sortedItems = items
  .map((fruit) => {
    vitamins = fruit.contains;
    let vitaminsArray = vitamins.replace(/\s*,\s*/g, ",").split(",");
    let noOfVitamins = vitaminsArray.length;
    fruit["noOfVitamins"] = noOfVitamins;
    return fruit;
  })
  .sort(function (currentElement, nextElement) {
    if (currentElement.noOfVitamins > nextElement.noOfVitamins) {
      return 1;
    } else if (currentElement.noOfVitamins < nextElement.noOfVitamins) {
      return -1;
    } else {
      return 0;
    }
  });

// console.log(sortedItems);
